package app;

import cucumber.api.CucumberOptions;
import fmk.boot.test.AbstractKarateTest;
import org.springframework.boot.test.context.SpringBootTest;

@CucumberOptions(features = "classpath:features")
@SpringBootTest(
    classes = Application.class,
    webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class KarateTest extends AbstractKarateTest {}
