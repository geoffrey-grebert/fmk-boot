package app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hello {

  private static final Logger LOG = LoggerFactory.getLogger(Hello.class);

  @Autowired ApplicationContext appCtx;

  @GetMapping("/")
  public String hello() {
    Environment environment = appCtx.getBean(Environment.class);
    LOG.info(
        "Environment's Indirect secret property: {}",
        environment.getProperty("spring.datasource.password"));

    return "Hello doudou";
  }
}
