package fmk.boot.core;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.context.annotation.ComponentScan;

/**
 * Autoload framework configurations
 *
 * @author Geoffrey GREBERT
 * @since 1.0.0
 */
@ComponentScan(basePackages = {"app", "fmk"})
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FmkBootApplication {}
