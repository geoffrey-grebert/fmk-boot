package fmk.boot.test;

import static org.junit.Assert.assertTrue;

import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public abstract class AbstractKarateTest {

  protected Integer parallel = 1;

  protected String env = "ci";

  @Test
  public void testParallel() {
    System.setProperty("karate.env", this.env);

    Results results = Runner.parallel(getClass(), this.parallel, "target/surefire-reports");
    generateReport(results.getReportDir(), this.env);
    assertTrue(results.getErrorMessages(), results.getFailCount() == 0);
  }

  public static void generateReport(String karateOutputPath, String env) {
    Collection<File> jsonFiles =
        FileUtils.listFiles(new File(karateOutputPath), new String[] {"json"}, true);

    List<String> jsonPaths = new ArrayList(jsonFiles.size());

    jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));

    Configuration config = new Configuration(new File("target"), env);
    ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);

    reportBuilder.generateReports();
  }
}
